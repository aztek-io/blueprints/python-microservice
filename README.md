# Python Example Microservice Template

## Items in this repo
* Unit Tests
* helmfile
* templated CI
* Dockerfile
* requirements.txt

## CI Configurations needed
* Docker repo created and specified with environment variable `DOCKER_REPO`
  - example would be `/aztek/example`
* Docker registry defined with `DOCKER_REGISTRY` (if not pushing to docker hub)
  - example would be `123456789012.dkr.ecr.us-west-2.amazonaws.com`
