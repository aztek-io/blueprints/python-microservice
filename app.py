#!/usr/bin/env python3
# pylint: disable=logging-format-interpolation

'''
    Microservice to listen for text alerts
'''
import logging
import os
import sys
import json
import statsd

from flask import Flask, Response

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Global Variables ##################
#######################################

APP   = Flask(__name__)
HOST  = os.environ.get('HOST', '0.0.0.0')
PORT  = os.environ.get('PORT', 3000)
DEBUG = os.environ.get('DEBUG', True)

TRADING_ALERT = os.environ.get('TRADING_ALERT', 'A trade has been issued')
MESSAGING_MS  = os.environ.get('MESSAGING_MS', 'messaging')

ALL_METHODS   = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH']

STATSD_HOST   = os.environ.get('STATSD_HOST', 'localhost')
STATSD_PORT   = os.environ.get('STATSD_PORT', 8125)
STATSD        = statsd.StatsClient(STATSD_HOST, STATSD_PORT)

STATSD_PREFIX = os.environ.get('MICROSERVICE_NAME', 'unamed')

#######################################
### Main Function #####################
#######################################

def main():
    '''
        Main function
    '''
    logger.info('Starting Application')

    STATSD.incr('{}.start'.format(STATSD_PREFIX))

    try:
        APP.run(debug=DEBUG, host=HOST, port=PORT)
    except OSError as os_error:
        STATSD.incr('{}.os_error'.format(STATSD_PREFIX))
        fatal(os_error)


    STATSD.incr('{}.shutdown'.format(STATSD_PREFIX))
    logger.info('Exiting Application')


#######################################
### Generic Functions #################
#######################################

def fatal(message, code=1):
    '''
        Exit if error
    '''
    logger.critical(message)
    logger.info('Exiting Application')
    sys.exit(code)


def configure_logging(stdout=True, log_format=None):
    '''
        Set up logging.
    '''
    if not log_format:
        log_format="%(asctime)s - %(levelname)s - %(message)s"

    formatter = logging.Formatter(log_format)

    if stdout:
        s_handler = logging.StreamHandler(sys.stdout)

        s_handler.setFormatter(formatter)
        logger.addHandler(s_handler)
        logger.debug('Configured logging to stdout.')


#######################################
### API Routes ########################
#######################################

@APP.route("/example", methods=['GET', 'POST'])
def sms_reply():
    """
        This is an example microservice.
    """
    status  = 200
    payload = response_generator(
        message='Hello, there.',
        status=status
    )

    STATSD.incr('{}.call'.format(STATSD_PREFIX))

    return Response(response=payload, status=status)


@APP.route("/healthz", methods=['GET'])
def healthz():
    """
        Respond to health check.
    """
    status  = 299
    payload = response_generator(
        message='Server listening.',
        status=status
    )

    STATSD.incr('{}.healthz'.format(STATSD_PREFIX))

    return Response(response=payload, status=status)


@APP.route('/', methods=ALL_METHODS)
def homepage():
    '''
        Home page route.
    '''
    message = 'This should not have a kubernetes ingress.'
    status  = 200

    payload = response_generator(
        message=message,
        status=status
    )

    logger.warning(message)

    STATSD.incr('{}.web_root'.format(STATSD_PREFIX))

    return Response(response=payload, status=status)


#######################################
### Program specific Functions ########
#######################################

def response_generator(message=None, status=403):
    '''
        This generates the text response
    '''
    return json.dumps(
        {
            "status": status,
            "message": message
        },
        indent=4
    ) + '\n'


#######################################
### Execution #########################
#######################################

if __name__ == "__main__":
    configure_logging(stdout=True)
    main()
