# DOCKER_BUILDKIT=1 docker build . -t example:local && docker run -it -p 3000:3000 example:local

FROM python:3.9.0-alpine3.12

EXPOSE 3000
ENV AWS_DEFAULT_REGION=us-west-2

COPY ["requirements.txt", "."]

RUN pip install \
        --no-cache-dir \
        -r requirements.txt

COPY ["app.py", "/"]
ENTRYPOINT ["python", "/app.py"]
