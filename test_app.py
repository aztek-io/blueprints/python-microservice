#!/usr/bin/env python
'''
    Run some unit tests.
'''

import unittest
import app

class TestApp(unittest.TestCase):
    '''
        Testing app.py
    '''
    def test_fatal(self):
        '''
            Testing app.fatal()
        '''
        def test_exit(message, code=None):
            with self.assertRaises(SystemExit) as this:
                if code:
                    app.fatal(message=message, code=code)
                else:
                    app.fatal(message=message)

            if code:
                self.assertEqual(this.exception.code, code)
            else:
                self.assertEqual(this.exception.code, 1)

        test_exit('app.fatal() test 1')
        test_exit('app.fatal() test 2', 2)
        test_exit('app.fatal() test 3', 3)


if __name__ == '__main__':
    unittest.main()
